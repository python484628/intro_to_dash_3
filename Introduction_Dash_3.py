
# Author : Marion Estoup
# E-mail : marion_110@hotmail.fr
# October 2021

######################################### Third Introduction to Dash



############## Import libraries
import pandas as pd
import dash 
from dash import dcc
from dash import html 
from dash.dependencies import Input, Output




############################################## First example of dashboard

# Same exercice as the previous Introduction to Dash (number 2) with iris data but in another way !

# Open data and print it
df = pd.read_csv('http://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data')
print(df)

# Create columns
attributes = ['sepal-length', 'sepal-width', 'petal-length', 'petal-width', 'class']
df.columns = attributes


# Create the app
app = dash.Dash(__name__)

# Create the layout
app.layout = html.Div(children=[
    html.H1(children='IRIS FISHER', style={'textAlign':'center'}),
    
    # Create First Dropdown
    dcc.Dropdown(
        id='data1',
        options=[
            {'label':'sepal-length','value':'sepal-length'},
            {'label':'sepal-width','value':'sepal-width'},
            {'label':'petal-length','value':'petal-length'},
            {'label':'petal-width','value':'petal-width'}
            ],
        value='petal-width'
        ),
    
    # Create second dropdown
    dcc.Dropdown(
        id='data2',
        options=[
            {'label':'sepal-length','value':'sepal-length'},
            {'label':'sepal-width','value':'sepal-width'},
            {'label':'petal-length','value':'petal-length'},
            {'label':'petal-width','value':'petal-width'}
            ],
        value='petal-width'),
    
    # Create empty graph
    dcc.Graph(id='plot-main')]
    
    )
    
    
# Create callback to update the graph depending on the user inputs
@app.callback(Output(component_id='plot-main', component_property='figure'),
              [Input(component_id='data1', component_property='value'),
               Input(component_id='data2', component_property='value')])



# Create function to update the graph depending on the user inputs
def update_plot(data1_choice, data2_choice):
    
    labels = df['class']
    data1 = df[data1_choice]
    data2 = df[data2_choice]
    
    return{
        
        # Create graph
        'data': [{
            'x': df[df['class'] == 'Iris-virginica'][data1_choice],
            'y': df[df['class'] == 'Iris-virginica'][data2_choice],
            'name':'Iris-virginica',
            'mode':'markers',
            'opacity':0.7,
            'marker':{'size':15, 'line':{'width':0.5, 'color':'white'}}
            
            },{
                
                'x': df[df['class'] == 'Iris-setosa'][data1_choice],
                'y': df[df['class'] == 'Iris-setosa'][data2_choice],
                'name':'Iris-setosa',
                'mode':'markers',
                'opacity':0.7,
                'marker':{'size':15, 'line':{'width':0.5, 'color':'white'}}
                
                },{
                    
                    'x': df[df['class'] == 'Iris-versicolor'][data1_choice],
                    'y': df[df['class'] == 'Iris-versicolor'][data2_choice],
                    'name':'Iris-versicolor',
                    'mode':'markers',
                    'opacity':0.7,
                    'marker':{'size':15, 'line':{'width':0.5, 'color':'white'}}
                    
                    }],
                    'layout':{
                        'title':'IRIS interactive',
                        'xaxis':{
                        'title':data1_choice
                        },
                        'yaxis':{
                        'title':data2_choice
                        }
                        
                        }
                    }
                    
                    
# Run the app
if __name__ == '__main__':
    app.run_server(debug=False)










############################################## Second example of dashboard with real-time data

# Import library
import yfinance as yf



# Create the app
app = dash.Dash(__name__)

# Create layout of the app
app.layout=html.Div([
    html.H1("Following the prices in real-time"),
    dcc.Graph(id="live-graph",animate=True),
    dcc.Interval(
        id='graph-update',
        interval=1000 * 60 # Every minute the server will be update (60 seconds)
        
        )
    ]
    )


# Create callback to update graph depending on the inputs (new data in real-time)
@app.callback(Output("live-graph","figure"),
              [Input('graph-update','n_intervals')])


# Create function to update graph depending on the inputs
def update_graph_scatter(input_data): # 1 parameter because 1 input only
    
# Collect real-time data
    stock = 'AAPL'
    df=yf.download(stock,period="1d", interval="1m", group_by='ticker',progress=False)
    fig = {
        # Create graph
        "data": [
            {
                "x":df.index,
                "y":df["Close"],
                "type":"line",
                "name":stock,
                },
            ],
        "layout":{"title":stock+ " Prices in real-time"},
        }
    
    # Return graph
    return fig
    
# Run the app (you have to refresh the page to update data on the graph)
if __name__ == '__main__':
    app.run_server(debug=False)













